//
//  Permission Constant.swift
//  Tribe
//
//  Created by APPLE on 22/08/18.
//  Copyright © 2018 InnoEye. All rights reserved.
//

import Foundation

class PermissionsConstant {
    
    //Home
    static let PERMISSION_HOME_VIEW = "ROLE_TRIBE_HOME_view";
    static let PERMISSION_HOME_POST_CREATE = "ROLE_TRIBE_Home_PostCreate";
    static let PERMISSION_HOME_POST_LIKE = "ROLE_TRIBE_Home_PostLike";
    static let PERMISSION_HOME_POST_SHARE = "ROLE_TRIBE_Home_PostShare";
    static let PERMISSION_HOME_POST_COMMENT = "ROLE_TRIBE_Home_PostComment";
    
    //People
    static let PERMISSION_PEOPLE_VIEW = "ROLE_TRIBE_PEOPLE_view";
    static let PERMISSION_PEOPLE_FOLLOW = "ROLE_TRIBE_PEOPLE_Follow";
    static let PERMISSION_PEOPLE_POST_LIKE = "ROLE_TRIBE_PEOPLE_PostLike";
    static let PERMISSION_PEOPLE_COMMENT = "ROLE_TRIBE_PEOPLE_PostComment";
    static let PERMISSION_PEOPLE_POST_CREATE = "ROLE_TRIBE_PEOPLE_PostCreate";
    static let PERMISSION_PEOPLE_POST_SHARE = "ROLE_TRIBE_PEOPLE_PostShare";
    
    //space
    static let PERMISSION_SPACE_VIEW = "ROLE_TRIBE_SPACES_view";
    static let PERMISSION_SPACE_CREATE = "ROLE_TRIBE_SPACES_Create";
    static let PERMISSION_SPACE_FOLLOW = "ROLE_TRIBE_SPACES_Follow";
    static let PERMISSION_SPACE_POST_LIKE = "ROLE_TRIBE_SPACES_PostLike";
    static let PERMISSION_SPACE_COMMENT = "ROLE_TRIBE_SPACES_PostComment";
    static let PERMISSION_SPACE_POST_CREATE = "ROLE_TRIBE_SPACES_PostCreate";
    static let PERMISSION_SPACE_POST_SHARE = "ROLE_TRIBE_SPACES_PostShare";
    
    //Forum
    static let PERMISSION_FORUM_VIEW = "ROLE_TRIBE_FORUMS_view";
    static let PERMISSION_FORUM_TOPIC_CREATE = "ROLE_TRIBE_FORUMS_TopicCreate";
    static let PERMISSION_FORUM_TOPIC_SUBSCRIBE = "ROLE_TRIBE_FORUMS_TopicSubscribe";
    static let PERMISSION_FORUM_TOPIC_SHARE = "ROLE_TRIBE_FORUMS_TopicShare";
    static let PERMISSION_FORUM_TOPIC_ANSWER = "ROLE_TRIBE_FORUMS_TopicAnswer";
    static let PERMISSION_FORUM_CHANNEL_SUBSCRIBE = "ROLE_TRIBE_FORUMS_ChannelSubscribe";
    static let PERMISSION_FORUM_TOPIC_COMMENT = "ROLE_TRIBE_FORUMS_TopicComment";
    static let PERMISSION_FORUM_TOPIC_LIKE = "ROLE_TRIBE_FORUMS_TopicLike";
    static let PERMISSION_FORUM_TOPIC_ANSWER_LIKE = "ROLE_TRIBE_FORUMS_AnswerLike";
    static let PERMISSION_FORUM_TOPIC_ANSWER_COMMENT = "ROLE_TRIBE_FORUMS_AnswerComment";
    
    //WIKI
    static let PERMISSION_WIKI_VIEW = "ROLE_TRIBE_WIKI_view";
    static let PERMISSION_WIKI_ARTICLE_SUBSCRIBE = "ROLE_TRIBE_WIKI_ArticleSubscribe";
    static let PERMISSION_WIKI_ARTICLE_BOOKMARK = "ROLE_TRIBE_WIKI_ArticleBookmark";
    static let PERMISSION_WIKI_ARTICLE_VIEW = "ROLE_TRIBE_WIKI_ArticleView";
    static let PERMISSION_WIKI_ARTICLE_SHARE = "ROLE_TRIBE_WIKI_ArticleShare";
    static let PERMISSION_WIKI_DISCUSSION_COMMENT = "ROLE_TRIBE_WIKI_DiscussionComment";
    static let ROLE_TRIBE_WIKI_DELETE = "ROLE_TRIBE_WIKI_PUBLISHREQUEST"
    
    //MY Document
    
    static let PERMISSION_DOCUMENT_CREATE = "ROLE_TRIBE_MYDOCUMENTS_CreateFolder";
    static let PERMISSION_DOCUMENT_UPLOAD = "ROLE_TRIBE_MYDOCUMENTS_UploadFiles";
    static let PERMISSION_DOCUMENT_SHARE = "ROLE_TRIBE_MYDOCUMENTS_Share";
    static let PERMISSION_DOCUMENT_VIEW = "ROLE_TRIBE_MYDOC_view";
    static let  PERMISSION_DOCUMENT_DOWNLOAD = "ROLE_TRIBE_MYDOCUMENTS_Download";
    
    static let  PERMISSION_CREATE_FOLDER_INSIDE_DM = "PERMISSION_FOR_CREATE_FOLDER_FILE_INSIDE_DM";

    
    
    //TOOLS
    static let PERMISSION_TOOLS_VIEW = "ROLE_Tool_Permission";
    static let PERMISSION_VENDOR_EXPENSE_VIEW = "ROLE_Vendor_Expense_Approval_App_Permission";
    
    //SITEFORGE
    static let PERMISSION_MY_TASK_VIEW = "ROLE_My_Task_Permission";
    static let PERMISSION_MAINTENANCE_VIEW="ROLE_Maintainence_Permission"
   
    static let PERMISSION_TICKET_VIEW="ROLE_Tickets_Permission"
    static let PERMISSION_CREATE_TICKETS = "ROLE_Create_Ticket_Permission";
//    static let PERMISSION_ADD_ATTACHMENT_TICKET = "ROLE_Add_Attachment_Permission";
    static let PERMISSION_ADD_COMMENT_TICKET = "ROLE_Add_Comment_Permission";
    
    static let PERMISSION_PROJECT_DASHBOARD_VIEW = "ROLE_Project_Dashboard_Permission"
    // static let PERMISSION_TICKET_DASHBOARD_VIEW = "ROLE_Tickets_Dashboard_Permission"
    static let PERMISSION_TICKET_DASHBOARD_VIEW = "TM_ROLE_SUMMARY_view"
    static let PERMISSION_ASSIGNMENT_DASHBOARD_VIEW = "ROLE_Assignment_Dashboard_Permission"
    static let ROLE_PMO_Assignment_Dashboard_Permission = "ROLE_PMO_Assignment_Dashboard_Permission"
    
    static let PERMISSION_TIMESHEET_VIEW = "ROLE_Timesheet_Permission";
    static let PERMISSION_MYTIMESHEET_VIEW = "ROLE_MY_TIMESHEET_TAB_Permission"
    
    static let ROLE_Calendar_Permission = "ROLE_Calendar_Permission"

    static let ROLE_Risk_Permission = "ROLE_Risk_Permission"
    static let ROLE_Add_Risk_Permission = "ROLE_Add_Risk_Permission"
    static let ROLE_Edit_Risk_Permission = "ROLE_Edit_Risk_Permission"
    
    static let PERMISSION_KC_VIEW="ROLE_Knowledge_Center_Permission"
    static let PERMISSION_KNOWLEDGE_CENTER_OVERVIEW_VIEW = "ROLE_Overview_Knowledge_Center_Permission";
    static let PERMISSION_KNOWLEDGE_CENTER_FAQ_VIEW = "ROLE_FAQ_Knowledge_Center_Permission";
    static let PERMISSION_KNOWLEDGE_CENTER_AUDIO_VIDEO_VIEW = "ROLE_Audio_Video_Knowledge_Center_Permission";
    static let PERMISSION_KNOWLEDGE_CENTER_DOCUMENTS_VIEW = "ROLE_Documents_Knowledge_Center_Permission";
    static let PERMISSION_KNOWLEDGE_CENTER_CERTIFICATION_VIEW = "ROLE_Certification_Knowledge_Center_Permission";
    
    //Dashboard
    
    static let PERMISSION_TRIBE_DASHBOARD = "ROLE_TRIBE_SITE_DASHBOARD_VIEW"
     static let PERMISSION_TRIBE_EXECUTIVE_DASHBOARD = "ROLE_TRIBE_EXECUTIVE_DASHBOARD_view"
    // Tickets
//    static let PERMISSION_COMMON_TICKET_CREATE = "ROLE_COMMONTICKETS_CREATE"
//    static let PERMISSION_COMMON_TICKET_VIEW = "ROLE_COMMON_TICKET_view"
    
    static let PERMISSION_COMMON_TICKET_CREATE = "TM_ROLE_TROUBLE_TICKETS_create"
    static let PERMISSION_COMMON_TICKET_VIEW = "TM_ROLE_TROUBLE_TICKETS_view"
    static let PERMISSION_COMMON_TICKET_ALL = "TM_ROLE_TROUBLE_ALL_TICKETS_view"
    static let PERMISSION_COMMON_TICKET_GROUP = "TM_ROLE_GROUP_TICKETS_view"
    static let PERMISSION_COMMON_TICKET_MY = "TM_ROLE_MY_TICKETS_view"
    
    // SXC Monitoring Duty
    static let PERMISSION_SXC_MONITORING_DUTY = "TRIBE_SXC_MONITORING_DUTY_PERMISSION"
    static let PERMISSION_VIEW_SXC_MONITORING_DUTY = "TRIBE_SXC_MONITORING_VIEW_PERMISSION"
    
    //ChangeRequest
    static let PERMISSION_VIEW_CHANGE_REQUEST = "TRIBE_CHANGE_REQUEST_VIEW_PERMISSION"
    
}
