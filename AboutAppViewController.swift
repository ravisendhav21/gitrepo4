//
//  AboutAppViewController.swift
//  Tribe
//
//  Created by Apple on 01/03/19.
//  Copyright © 2019 InnoEye. All rights reserved.
//

import UIKit

class AboutAppViewController: BaseViewController {

    @IBOutlet var lblVersion: UILabel!
    @IBOutlet var lblCopyright: UILabel!
    @IBOutlet var lblRightsReserved: UILabel!

    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.updateSubtitle(title: Utility.sharedInstance.getLanguageConvertedString(string: "About"))
        // Do any additional setup after loading the view.
        let InfoPlist = Bundle.main.infoDictionary!
        let Currentversion = InfoPlist["CFBundleShortVersionString"] as! String
        lblVersion.text = Utility.sharedInstance.getLanguageConvertedString(string: "Version") + " : " + Currentversion
        lblCopyright.text = Utility.sharedInstance.getLanguageConvertedString(string: "Copyright") + " © 2019"
        lblRightsReserved.text = Utility.sharedInstance.getLanguageConvertedString(string: "All Rights Reserved")
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
